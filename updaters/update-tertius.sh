#!/bin/sh
REPOS="/root/git/tertius"

if [ ${UID} == 0 ]
then
    echo "Updating ${REPOS}"
    cd ${REPOS}
    git pull
else
    echo "No root, no pull."
    exit 1
fi
