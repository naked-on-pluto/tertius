#!/bin/bash
# If you need to backup new files/folders, add them in ./to-backup
# One item per line, absolute paths, and nothing that is version 
# controlled if possible

if [ ! -f ./to-backup -o ! -d ./archive ]
then

    echo "Please run me from inside the backup folder!"
    exit 1

else

    # commit new backup directories
    git commit -q -m "auto commit: to-backup" to-backup

    # rsync with latest versions
    cat to-backup | while read SOURCE
    do
        echo "Backing up ${SOURCE}"
        TARGET=`echo ${SOURCE} | sed 's/.\(.*\)/\1/'`
        rsync -R -a -P ${SOURCE} archive/
    done

    # One commit per file backup/update
    # K.I.S.S. style :)
    for FILE in `find archive -type f`
    do
        echo "Inspecting ${FILE}:"
	git add ${FILE}
	git commit -q -m "auto commit: ${FILE}" ${FILE}
    done

    # Optionnal push
    if [ "${FORCE_PUSH}" == "True" ]
    then
        git push
    else

        while true
        do
            read -p "Shall we push? (Y/N)? "
            if [ "${REPLY}" == "Y" ]
            then
                echo "OK. Pushing."
                git push
                break
            elif [ "${REPLY}" == "N" ]
            then
                echo "OK. Not Pushing."
                break
            else
                echo "Answer Y or N."
                read -p "Shall we push sweetie? (Y/N)? "
            fi
        done
    fi
fi

exit 0
