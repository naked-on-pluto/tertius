<?
    /* config_local.php - Local configuration file
       -------------------------------------------
       In this file you can place your custom configuration, instead of editing
       config_inc.php directly, although this is possible to.

       Please note: Everything you set here has precedence over settings defined in the config_inc.php. 
    */

	$g_path = 'http://we-are.naked-on-pluto.net/';

	# --- anonymous login -----------
	#$g_allow_anonymous_login = ON;
	#$g_anonymous_account = 'anonymous';
 
	# Optionally, if you want to use blank email addresses
	#$g_allow_blank_email = ON;

	#Captchas
	$g_signup_use_captcha = ON;

?>
